#!/bin/bash
API_KEY=------- # Bandcamp doesn't give out API keys no more

# API module paths
API_TRACK="http://api.bandcamp.com/api/track/3/info?key=$API_KEY" # track module api url
API_ALBUM="http://api.bandcamp.com/api/album/2/info?key=$API_KEY"
API_BAND="http://api.bandcamp.com/api/band/3/info?key=$API_KEY"

#misc
DOWNLOAD_PATH="/$HOME/Downloads/" # the path where the mp3 should go when downloaded
PREFIX="[bandcamp-downloader]"

function get_url {
  curl -s --user-agent 'Chrome/37' $1
}

#initializes the json path and module constants
function init {
  track_id=$(get_url $1 | grep -oE "track_id :.[0-9]*" | tr -d '[a-z]/:/_/ /')
  TRACK="$API_TRACK$TRACK&track_id=$track_id&debug"

  if [ -z $track_id ]; then
    echo "Make sure the URL is from bandcamp.com"
    exit
  else
    album_id=$(get_val $TRACK "album_id")
    ALBUM="$API_ALBUM$ALBUM&album_id=$album_id&debug"

    band_id=$(get_val $TRACK "band_id")
    BAND="$API_BAND$BAND&band_id=$band_id&debug"
  fi
}

#returns a value off the track json file
function get_val {
  get_url $1 | jsawk "return this.$2" # the value argument to get
}

function download_song {
  local stream_url=$(get_val $TRACK "streaming_url")
  local title=$(get_val $TRACK "title")
  cd $DOWNLOAD_PATH
  echo "$PREFIX Downloading '$title'.mp3 to: '$DOWNLOAD_PATH'..."
  lynx -source $stream_url > "${title}".mp3
  configurate_tags "${title}".mp3
  echo "$PREFIX Successfully downloaded $title.mp3"
}

function configurate_tags {
  echo "$PREFIX Writing tags.."
  local mp3=$1
  local track_num=$(get_val $TRACK "number")
  local lyrics=$(get_val $TRACK "lyrics")
  local artist=$(get_val $BAND "name")
  local album=$(get_val $ALBUM "title")
  local track_title=$(get_val $TRACK "title")
  local release_date=$(get_val $ALBUM "release_date")
  local year=$(date -r $release_date '+%Y')
  local album_art=$(get_val $ALBUM "large_art_url")
  eyeD3 --remove-all $mp3 &>/dev/null
  eyeD3 --album=$album \
  --title=$track_title \
  --artist=$artist \
  --year=$year $mp3 &>/dev/null
  lynx -source $album_art > art.jpg
  eyeD3 --to-v2.3 --add-image="art.jpg":FRONT_COVER $mp3 &>/dev/null
  rm -rf art.jpg
  echo "$PREFIX Finished writing tags!"
}

echo "+--------------------------------------------------+
|          bandcamp downloader by global           |
+------+-------------------------------------------+
       |
       +-->  usage: bandcamp-downloader http://someband.bandcamp.com/track/cool-song
"

if [ $# -eq 0 ]; then
  echo "track page not specified!"
else
  init $1
  download_song
fi
exit
