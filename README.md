# Bandcamp-Downloader #

A shell script that downloads specified tracks off bandcamp.com

### Libraries ###
* [**Lynx**](http://lynx.browser.org/)
* [**eyeD3**](http://eyed3.nicfit.net/)

### Usage ###

```
#!bash

bandcamp-downloader http://coolband.bandcamp.com/track/a-song-u-like
```


